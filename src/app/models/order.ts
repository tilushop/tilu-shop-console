import {User} from "./user";
import {OrderedItem} from "./orderedItem";
export class Order {
  id: string;
  user: User;
  items: OrderedItem[];
  notes: string;
  createdDate: Date;
  updatedDate: Date;
}