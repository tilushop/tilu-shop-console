export class Item {
  id: string;
  name: string;
  url: string;
  imageUrl: string;
  code: string;
  country: string;
  weight: number;
  description: string;
  basePrice: number;
  usualPrice: number;
  promotionPrice: number;
  active: boolean;
  orderQuantity: number;
  category: string;
  color: string;
  inventoryQuantity: number;
  reservedQuantity: number;
  status: string;
  createdDate: Date;
  updatedDate: Date;
}
