export class User {
  id: string;
  name: string;
  facebook: string;
  email: string;
  birthDay: Date;
  gender: string;
  phone: number;
  address: string;
  createdDate: Date;
  updatedDate: Date;
}