export class OrderedItem {
  id: string;
  description: string;
  orderQuantity: number;
  pricePerItem: number;

  constructor(id: string,
              description: string,
              orderQuantity: number,
              pricePerItem: number) {
    this.id = id;
    this.description = description;
    this.orderQuantity = orderQuantity;
    this.pricePerItem = pricePerItem;
  }
}