import {NgModule} from "@angular/core";
import {RouterModule, Routes, PreloadAllModules} from "@angular/router";
import {ItemEditComponent} from "./item/item-new.component";
import {CanActivateGuard} from "./item/can-activate.guard";
import {CanDeactivateGuard} from "./item/can-deactivate.guard";

const app_routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/items/ordered', canActivate: [CanActivateGuard]},

  {path: 'items/ordered', loadChildren: 'app/items/ordered-items.module#OrderedItemsModule', canActivate: [CanActivateGuard]},
  {path: 'items/ordered/:id', loadChildren: 'app/item/item.module#ItemModule', canActivate: [CanActivateGuard]},
  {path: 'items/all', loadChildren: 'app/items/all-items.module#AllItemsModule', canActivate: [CanActivateGuard]},
  {path: 'items/all/:id', loadChildren: 'app/item/item.module#ItemModule', canActivate: [CanActivateGuard]},
  {path: 'items/new', component: ItemEditComponent, canActivate: [CanActivateGuard]},

  {path: 'orders', loadChildren: 'app/orders/orders.module#OrdersModule', canActivate: [CanActivateGuard]},

  {path: 'users', loadChildren: 'app/users/users.module#UsersModule', canActivate: [CanActivateGuard]},
  {path: 'users/:id', loadChildren: 'app/user/user.module#UserModule', canActivate: [CanActivateGuard]},

  {path: '**', pathMatch: 'full', redirectTo: '/items/ordered', canActivate: [CanActivateGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(app_routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule],
  providers: [CanActivateGuard, CanDeactivateGuard]
})
export class AppRoutingModule {
}
