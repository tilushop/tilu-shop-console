import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {ErrorHandler} from "../../utils/ErrorHandler";
import {Observable} from "rxjs";
import {IPagedResults} from "../../shared/interfaces";
import {Item} from "../../models/item";

@Injectable()
export class ItemService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private itemServiceUrl = 'http://localhost:9001/items';
  private orderServiceUrl = 'http://localhost:9002/orders';

  constructor(private http: Http) {
  }

  getItemsPage(page: number, pageSize: number): Observable<IPagedResults<Item[]>> {
    return this.http.get(`${this.itemServiceUrl}/${page}/${pageSize}`)
    .map(response => {
          let allItems = <Item[]> response.json();
          return {
            // results: allItems.slice(page * pageSize, (page + 1) * pageSize),
            results: allItems,
            totalRecords: allItems.length
          }
        }
    )
    .catch(ErrorHandler.handleError);
  }

  countAllItems(): Observable<number> {
    return this.http.get(`${this.itemServiceUrl}/count`)
    .map(res => <number> res.json())
    .catch(ErrorHandler.handleError);
  }

  getItem(id: string): Observable<Item> {
    const url = `${this.itemServiceUrl}/${id}`;

    return this.http.get(url)
    .map(res => <Item> res.json())
    .catch(ErrorHandler.handleError);
  }

  remove(id: string): Promise<void> {
    const url = `${this.itemServiceUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(ErrorHandler.handleError);
  }

  create(item: Item): Observable<string> {
    return this.http
    .post(this.itemServiceUrl, JSON.stringify(item), {headers: this.headers})
    .map(res => <string> res.json())
    .catch(ErrorHandler.handleError);
  }

  update(item: Item): Observable<void> {
    const url = `${this.itemServiceUrl}/${item.id}`;
    return this.http
    .put(url, JSON.stringify(item), {headers: this.headers})
    .catch(ErrorHandler.handleError);
  }

  getAllItemCategories(): Observable<string[]> {
    return this.http
    .get(`${this.itemServiceUrl}/categories`)
    .map(res => <string[]> res.json())
    .catch(ErrorHandler.handleError);
  }

  getAllItemColors(): Observable<string[]> {
    return this.http
    .get(`${this.itemServiceUrl}/colors`)
    .map(res => <string[]> res.json())
    .catch(ErrorHandler.handleError);
  }
}

