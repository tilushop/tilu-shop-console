import {Injectable} from "@angular/core";
import {Item} from "../../models/item";

@Injectable()
export class FilterService {

  constructor() {
  }

  filter(items: Item[], dateRangeFilter: DateRange) {
    let fromMillis = new Date(dateRangeFilter[0]).getTime();
    let toMillis = new Date(dateRangeFilter[1]).getTime();

    return items.filter((item: Item) => {
      let itemCreatedTime = new Date(item.createdDate).getTime();
      return itemCreatedTime >= fromMillis && itemCreatedTime <= new Date(toMillis + 24 * 3600 * 1000).getTime();
    });
  }
}