import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {ErrorHandler} from "../../utils/ErrorHandler";
import {Observable} from "rxjs";
import {IPagedResults} from "../../shared/interfaces";
import {User} from "../../models/user";
import {Order} from "../../models/order";

@Injectable()
export class UserService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private userServiceUrl = 'http://localhost:9000/users';
  private orderServiceUrl = 'http://localhost:9002/orders';

  constructor(private http: Http) {
  }

  getUsersPage(page: number, pageSize: number): Observable<IPagedResults<User[]>> {
    return this.http.get(this.userServiceUrl)
    .map(response => {
          let allUsers = <User[]> response.json();
          return {
            results: allUsers.slice(page * pageSize, (page + 1) * pageSize),
            totalRecords: allUsers.length
          }
        }
    )
    .catch(ErrorHandler.handleError);
  }

  getUser(id: string): Observable<User> {
    const url = `${this.userServiceUrl}/${id}`;

    return this.http.get(url)
    .map(res => <User> res.json())
    .catch(ErrorHandler.handleError);
  }

  remove(id: string): Promise<void> {
    const url = `${this.userServiceUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(ErrorHandler.handleError);
  }

  create(user: User): Observable<string> {
    return this.http
    .post(this.userServiceUrl, JSON.stringify(user), {headers: this.headers})
    .catch(ErrorHandler.handleError);
  }

  update(user: User): Observable<void> {
    const url = `${this.userServiceUrl}/${user.id}`;
    return this.http
    .put(url, JSON.stringify(user), {headers: this.headers})
    .catch(ErrorHandler.handleError);
  }

  getUserOrders(user: User): Observable<Order[]> {
    const url = `${this.orderServiceUrl}/user?phone=${user.phone}`;

    return this.http.get(url)
    .map(res => <User> res.json())
    .catch(ErrorHandler.handleError);
  }
}

