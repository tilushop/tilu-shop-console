import {Injectable} from "@angular/core";

@Injectable()
export class DialogService {

  promise: Promise<boolean>;
  message: string = 'Is it OK?';

  confirm(message?: string) {
    if (message) {
      this.message = message
    }
    this.promise = new Promise<boolean>(DialogService.resolver);
    return this.promise;
  };

  static resolver(resolve: any) {
    return resolve(window.confirm('Is it OK?'));
  }

}