import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {ErrorHandler} from "../../utils/ErrorHandler";
import {Observable} from "rxjs";
import {IPagedResults} from "../../shared/interfaces";
import {Order} from "../../models/order";

@Injectable()
export class OrderService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private orderServiceUrl = 'http://localhost:9002/orders';

  constructor(private http: Http) {
  }

  getOrdersPage(page: number, pageSize: number): Observable<IPagedResults<Order[]>> {
    return this.http.get(this.orderServiceUrl)
    .map(response => {
          let allOrders = <Order[]> response.json();
          return {
            results: allOrders.slice(page * pageSize, (page + 1) * pageSize),
            totalRecords: allOrders.length
          }
        }
    )
    .catch(ErrorHandler.handleError);
  }

  countAllOrders(): Observable<number> {
    return this.http.get(`${this.orderServiceUrl}/count`)
    .map(res => <number> res.json())
    .catch(ErrorHandler.handleError);
  }

  getOrder(id: string): Promise<Order> {
    const url = `${this.orderServiceUrl}/${id}`;
    return this.http.get(url)
    .toPromise()
    .then(response => <Order> response.json())
    .catch(ErrorHandler.handleError);
  }

  create(order: Order): Promise<string> {
    const url = `${this.orderServiceUrl}`;
    return this.http
    .post(url, JSON.stringify(order), {headers: this.headers})
    .toPromise()
    .then(res => res.json().data.id)
    .catch(ErrorHandler.handleError);
  }

  update(order: Order): Promise<void> {
    const url = `${this.orderServiceUrl}/${order.id}`;
    return this.http
    .put(url, JSON.stringify(order), {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(ErrorHandler.handleError);
  }

  getOrdersByItem(itemId: string, from: number, to: number): Observable<Order[]> {
    const url = `${this.orderServiceUrl}/filter?id=${itemId}&from=${from}&to=${to}`;
    return this.http
    .get(url)
    .map(res => <Order> res.json())
    .catch(ErrorHandler.handleError);
  }
}

