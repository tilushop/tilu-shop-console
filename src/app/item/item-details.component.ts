import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {ItemService} from "../core/services/item.service";
import {Item} from "../models/item";

@Component({
  moduleId: module.id,
  selector: 'cm-item-details',
  templateUrl: 'item-details.component.html',
  styleUrls: ['item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {

  item: Item;

  constructor(private route: ActivatedRoute, private itemService: ItemService) {
  }

  ngOnInit() {
    this.route.parent.params.subscribe((params: Params) => {
      let id = params['id'];
      this.itemService.getItem(id).subscribe((item: Item) => {
        this.item = item;
      });
    });
  }
}