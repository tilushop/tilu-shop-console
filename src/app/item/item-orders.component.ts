import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {Item} from "../models/item";
import {Order} from "../models/order";
import {OrderService} from "../core/services/order.service";

@Component({
  moduleId: module.id,
  selector: 'cm-item-orders',
  templateUrl: 'item-orders.component.html'
})
export class ItemOrdersComponent implements OnInit {

  orders: Order[] = [];
  item: Item;
  id: string;
  dateRangeFilter: DateRange;

  constructor(private route: ActivatedRoute, private orderService: OrderService) {
  }

  ngOnInit() {
    this.route.parent.params.subscribe((params: Params) => {
      this.id = params['id'];
    });

    this.route.queryParams.subscribe(params => {
      this.dateRangeFilter = params["dateRangeFilter"];
    });

    // TODO: Fix backend to handle millis
    let from = new Date(this.dateRangeFilter[0]).getTime()/1000;
    let to = new Date(this.dateRangeFilter[1]).getTime()/1000;
    this.orderService.getOrdersByItem(this.id, from, to).subscribe((orders: Order[]) => {
      this.orders = orders;
    });
  }
}