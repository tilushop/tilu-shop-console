import { NgModule }      from '@angular/core';

import { SharedModule }   from '../shared/shared.module';
import { ItemRoutingModule } from './item-routing.module';

@NgModule({
  imports:      [ ItemRoutingModule, SharedModule ],
  declarations: [ ItemRoutingModule.components ]
})
export class ItemModule { }