import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ItemDetailsComponent} from "./item-details.component";
import {ItemOrdersComponent} from "./item-orders.component";
import {ItemComponent} from "./item.component";

const routes: Routes = [
  {
    path: '',
    component: ItemComponent,
    children: [
      {path: 'details', component: ItemDetailsComponent},
      {path: 'orders', component: ItemOrdersComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemRoutingModule {
  static components = [ItemComponent, ItemDetailsComponent, ItemOrdersComponent];
}

