import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute, Params} from "@angular/router";
import {Validators, FormBuilder, FormGroup} from "@angular/forms";
import {ModalService, IModalContent} from "../core/modal/modal.service";
import {GrowlerService, GrowlerMessageType} from "../core/growler/growler.service";
import {ItemService} from "../core/services/item.service";
import {Item} from "../models/item";

@Component({
  moduleId: module.id,
  selector: 'cm-item-new',
  templateUrl: 'item-new.component.html',
  styleUrls: ['item-new.component.css']
})
export class ItemEditComponent implements OnInit {

  item: Item;
  itemCategories: string[] = [];
  selectedCategory: string;
  submitted: boolean;
  private itemForm: FormGroup;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private itemService: ItemService,
              private growler: GrowlerService,
              private modalService: ModalService,
              public fb: FormBuilder) {
  }

  ngOnInit() {
    this.route.parent.params.subscribe((params: Params) => {
      let id = params['id'];
      if (id !== 0) {
      }
    });

    this.itemService
    .getAllItemCategories()
    .subscribe((itemCategories: string[]) => {
      this.itemCategories = this.itemCategories.concat(itemCategories)
    });

    this.itemForm = this.fb.group({
      name: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
      url: [''],
      imageUrl: [''],
      country: ['', [<any>Validators.required]],
      weight: ['', [<any>Validators.required, <any>Validators.min(0)]],
      description: ['', [<any>Validators.required, <any>Validators.minLength(10)]],
      basePrice: ['', [<any>Validators.required, <any>Validators.min(0)]],
      usualPrice: ['', [<any>Validators.required, <any>Validators.min(0)]],
      promotionPrice: [''],
      category: [''],
      color: ['', [<any>Validators.required]],
      inventoryQuantity: ['', [<any>Validators.required]]
    });
  }

  onCategorySelect(selectedCategory: any) {
    this.selectedCategory = selectedCategory;
  }

  addItem(itemForm: any, isValid: boolean) {
    this.submitted = true;
    if (isValid) {
      let item = new Item();
      item.name = itemForm.name;
      item.url = itemForm.url;
      item.imageUrl = itemForm.imageUrl;
      item.country = itemForm.country;
      item.weight = itemForm.weight;
      item.description = itemForm.description;
      item.basePrice = itemForm.basePrice;
      item.usualPrice = itemForm.usualPrice;
      if (itemForm.promotionPrice) {
        item.promotionPrice = itemForm.promotionPrice;
      }
      item.category = this.selectedCategory;
      item.color = itemForm.color;
      item.inventoryQuantity = itemForm.inventoryQuantity;
      item.reservedQuantity = 0;
      item.status = "New";

      this.itemService.create(item).subscribe((id: string) => {
        this.growler.growl("The item is added successfully", GrowlerMessageType.Success);
        this.submitted = false;
        this.itemForm.reset();
      });
    }
  }

  cancel(event: Event) {
    event.preventDefault();
    //Route guard will take care of showing modal dialog service if data is dirty
    this.router.navigate(['/items']);
  }

  canDeactivate(): Promise<boolean> | boolean {
    if (!this.itemForm.dirty) {
      return true;
    }

    //Dirty show display modal dialog to user to confirm leaving
    const modalContent: IModalContent = {
      header: 'Lose Unsaved Changes?',
      body: 'You have unsaved changes! Would you like to leave the page and lose them?',
      cancelButtonText: 'Cancel',
      OKButtonText: 'Leave'
    };
    return this.modalService.show(modalContent);
  }
}