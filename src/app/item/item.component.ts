import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'cm-item',
  templateUrl: 'item.component.html'
})
export class ItemComponent implements OnInit {
  from: Date;
  to: Date;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.from = params["from"];
      this.to = params["to"];
    });
  }
}