import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable }    from 'rxjs/Observable';

import { ItemEditComponent } from './item-new.component';

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<ItemEditComponent> {

  canDeactivate(
    component: ItemEditComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    console.log(`CustomerId: ${route.parent.params['id']} URL: ${state.url}`);

    //Check with component to see if we're able to deactivate
    return component.canDeactivate();
  }
}