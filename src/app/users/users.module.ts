import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {UsersRoutingModule} from "./users-routing.module";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [UsersRoutingModule, SharedModule, ReactiveFormsModule],
  declarations: [UsersRoutingModule.components]
})
export class UsersModule {
}