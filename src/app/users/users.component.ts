import {Component, OnInit} from "@angular/core";
import {IPagedResults} from "../shared/interfaces";
import {User} from "../models/user";
import {UserService} from "../core/services/user.service";

@Component({
  moduleId: module.id,
  selector: 'cm-users',
  templateUrl: 'users.component.html',
  styleUrls: ['users.component.css']
})
export class UsersComponent implements OnInit {

  title: string;
  users: User[] = [];
  totalRecords: number = 0;
  pageSize: number = 10;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.title = 'All Users';
    this.getUsersPage(1);
  }

  pageChanged(page: number) {
    this.getUsersPage(page);
  }

  getUsersPage(page: number) {
    this.userService.getUsersPage((page - 1) * this.pageSize, this.pageSize)
    .subscribe((response: IPagedResults<User[]>) => {
          this.users = response.results;
          this.totalRecords = response.totalRecords;
        }, (err: any) => console.log(err)
    );
  }
}