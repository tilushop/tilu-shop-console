import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "./user.component";
import {UserOrdersComponent} from "./user-orders.component";
import {UserDetailsComponent} from "./user-details.component";

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {path: 'details', component: UserDetailsComponent},
      {path: 'orders', component: UserOrdersComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
  static components = [UserComponent, UserDetailsComponent, UserOrdersComponent];
}

