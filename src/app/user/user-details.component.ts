import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {UserService} from "../core/services/user.service";
import {User} from "../models/user";

@Component({
  moduleId: module.id,
  selector: 'cm-user-details',
  templateUrl: 'user-details.component.html',
  styleUrls: ['user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  user: User;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.route.parent.params.subscribe((params: Params) => {
      let id = params['id'];
      this.userService.getUser(id).subscribe((user: User) => {
        this.user = user;
      });
    });
  }
}