import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {Order} from "../models/order";
import {User} from "../models/user";
import {UserService} from "../core/services/user.service";

@Component({
  moduleId: module.id,
  selector: 'cm-user-orders',
  templateUrl: 'user-orders.component.html',
  styleUrls: ['user-orders.component.css']
})
export class UserOrdersComponent implements OnInit {
  orders: Order[] = [];

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.route.parent.params.subscribe((params: Params) => {
      this.userService.getUser(params['id']).subscribe((user: User) => {
        this.userService.getUserOrders(user).subscribe((orders: Order[]) => {
          this.orders = orders;
        });
      });
    });
  }
}