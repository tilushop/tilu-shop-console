import {Component, OnInit} from "@angular/core";
import {IPagedResults} from "../shared/interfaces";
import {OrderService} from "../core/services/order.service";
import {Order} from "../models/order";
import {DateHelperService} from "../core/services/date.service";

@Component({
  moduleId: module.id,
  selector: 'cm-items-orders',
  templateUrl: 'orders.component.html',
  providers: [DateHelperService]
})
export class OrdersComponent implements OnInit {
  orders: Order[];
  totalRecords: number = 0;
  pageSize: number = 10;

  constructor(private orderService: OrderService) {
  }

  ngOnInit() {
    this.getOrdersPage(1);
    this.orderService.countAllOrders()
    .subscribe((response: number) => {
          this.totalRecords = response;
        }, (err: any) => console.log(err)
    );
  }

  pageChanged(page: number) {
    this.getOrdersPage(page);
  }

  getOrdersPage(page: number) {
    this.orderService.getOrdersPage((page - 1) * this.pageSize, this.pageSize)
    .subscribe((response: IPagedResults<Order[]>) => {
      this.orders = response.results;
    });
  }

  calculateOrderTotalAmount(order: Order): number {
    return order.items.map(item => item.orderQuantity * item.pricePerItem).reduce((x, y) => x + y);
  }
}