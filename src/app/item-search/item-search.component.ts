import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import "rxjs/add/observable/of";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/switchMap";
import {ItemSearchService} from "./item-search.service";
import {Item} from "../models/item";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'item-search',
  templateUrl: './item-search.component.html',
  styleUrls: ['./item-search.component.css'],
  providers: [ItemSearchService]
})
export class ItemSearchComponent implements OnInit {
  items: Observable<Item[]>;
  private searchTerms = new Subject<string>();

  constructor(private itemSearchService: ItemSearchService, private router: Router) {
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.items = this.searchTerms
    .debounceTime(300)        // wait 300ms after each keystroke before considering the term
    .distinctUntilChanged()   // ignore if next search term is same as previous
    .switchMap(term => term ? this.itemSearchService.search(term) : Observable.of<Item[]>([]))
    .catch(error => {
      console.log(`Error in component ... ${error}`);
      return Observable.of<Item[]>([]);
    });
  }
}