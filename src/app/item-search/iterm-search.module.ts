import {NgModule} from "@angular/core";
import {ItemSearchComponent} from "./item-search.component";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [CommonModule],
  exports: [ItemSearchComponent],
  declarations: [ItemSearchComponent]
})
export class ItemSearchModule {
}