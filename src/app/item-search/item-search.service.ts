import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import {Item} from "../models/item";


@Injectable()
export class ItemSearchService {
  private itemServiceUrl = 'http://localhost:9001/items';

  constructor(private http: Http) {
  }

  search(term: string): Observable<Item[]> {
    console.log("Searching now...");
    return this.http
    .get(`${this.itemServiceUrl}/search?key=${term}`)
    .map(response => <Item[]> response.json())
    .catch((error: any) => {
      console.error('An friendly error occurred', error);
      return Observable.throw(error.message || error);
    });
  }
}
