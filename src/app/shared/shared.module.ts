import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {FilterTextboxModule} from "./filter-textbox/filter-textbox.module";
import {MapModule} from "./map/map.module";
import {PaginationModule} from "./pagination/pagination.module";
import {CapitalizePipe} from "./pipes/capitalize.pipe";
import {TrimPipe} from "./pipes/trim.pipe";
import {SortByDirective} from "./directives/sortby.directive";
import {DateFilterModule} from "./date-filter/date-filter.module";
import {ItemSearchModule} from "../item-search/iterm-search.module";
import {TruncatePipe} from "./pipes/truncate";

@NgModule({
  imports: [
    CommonModule,
    MapModule,
    FilterTextboxModule,
    DateFilterModule,
    PaginationModule,
    ItemSearchModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    CapitalizePipe,
    TrimPipe,
    TruncatePipe,
    SortByDirective,
    MapModule,
    FilterTextboxModule,
    DateFilterModule,
    PaginationModule,
    ItemSearchModule
  ],
  declarations: [
    CapitalizePipe,
    TrimPipe,
    TruncatePipe,
    SortByDirective
  ]
})
export class SharedModule {
}
