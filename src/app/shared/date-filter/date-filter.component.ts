import {Component, Output, EventEmitter, OnInit} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DatePipe} from "@angular/common";

@Component({
  moduleId: module.id,
  selector: 'date-filter',
  templateUrl: 'date-filter.component.html',
  styleUrls: ['date-filter.component.css']
})
export class DateFilterComponent implements OnInit {
  private dateFilterForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.dateFilterForm = this.fb.group({
      from: [''],
      to: [''],
    });

    let today = new Date();
    let todayStr = new DatePipe("US").transform(today, 'yyyy-MM-dd');
    let oneWeekAgo = today.setDate(today.getDate() - 6);
    let oneWeekAgoStr = new DatePipe("US").transform(oneWeekAgo, 'yyyy-MM-dd');
    this.dateFilterForm.setValue({from: oneWeekAgoStr, to: todayStr});

    this.sendDateFilterEvent(); // to pass date filter to the parent, i.e. ordered-items.component.ts
  }

  @Output() changed: EventEmitter<DateRange> = new EventEmitter<DateRange>();

  dateFilterChanged(event: any) {
    this.sendDateFilterEvent();
  }

  sendDateFilterEvent(): void {
    this.changed.emit([<Date> this.dateFilterForm.get('from').value, <Date>this.dateFilterForm.get('to').value]);
  }
}
