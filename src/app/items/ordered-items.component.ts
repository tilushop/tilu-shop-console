import {Component, OnInit} from "@angular/core";
import {IPagedResults} from "../shared/interfaces";
import {FilterService} from "../core/services/filter.service";
import {Item} from "../models/item";
import {ItemService} from "../core/services/item.service";

@Component({
  moduleId: module.id,
  selector: 'cm-items',
  templateUrl: 'ordered-items.component.html',
  styleUrls: ['items.component.css']
})
export class OrderedItemsComponent implements OnInit {

  title = "All Items";
  items: Item[] = [];
  filteredItems: Item[] = [];
  totalRecords: number = 0;
  pageSize: number = 10;
  dateRange: DateRange;

  constructor(private filterService: FilterService, private itemService: ItemService) {
  }

  ngOnInit() {
    this.getItemsPage(1);
    this.itemService.countAllItems()
    .subscribe((response: number) => {
          this.totalRecords = response;
        }, (err: any) => console.log(err)
    );
  }

  pageChanged(page: number) {
    this.getItemsPage(page);
  }

  getItemsPage(page: number) {
    this.itemService.getItemsPage(page - 1, this.pageSize)
    .subscribe((response: IPagedResults<Item[]>) => {
          this.items = this.filteredItems = response.results;
        }, (err: any) => console.log(err)
    );
  }

  dateFilterChanged(dateRange: DateRange) {
    this.dateRange = dateRange;
    this.filteredItems = this.filterService.filter(this.items, dateRange);
  }
}