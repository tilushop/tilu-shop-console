import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {OrderedItemsRoutingModule} from "./ordered-items-routing.module";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [OrderedItemsRoutingModule, SharedModule, ReactiveFormsModule],
  declarations: [OrderedItemsRoutingModule.components]
})
export class OrderedItemsModule {
}