import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {OrderedItemsComponent} from "./ordered-items.component";

const routes: Routes = [
  {
    path: '', component: OrderedItemsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderedItemsRoutingModule {
  static components = [OrderedItemsComponent];
}