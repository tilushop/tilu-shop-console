import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import {AllItemsRoutingModule} from "./all-items-routing.module";

@NgModule({
  imports: [AllItemsRoutingModule, SharedModule, ReactiveFormsModule],
  declarations: [AllItemsRoutingModule.components]
})
export class AllItemsModule {
}